//Class Component
//class Hello extends React.Component {
//El class component incluye el metodo render()
//render() {
//Dentro de return se acepta un solo elemento
//return (
//<div>
//<h1>Hola</h1>
//<h2>Mundo</h2>
//</div>
//)
//}
//}

//function component
//No puede usar importantes caracteristicas como: state, lifecycle methods
function Hello() {
  //El function component no necesita el metodo render solo el contenido de return
  return (
    <div>
      <h1>Hola</h1>
      <h1>Mundo</h1>
    </div>
  )
}

ReactDOM.render(<Hello />, document.getElementById("root"))
