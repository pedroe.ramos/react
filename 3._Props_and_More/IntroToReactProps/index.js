class App extends React.Component {
  render() {
    return (
      <div>
        {/* Datos que se pasan de nuestro componente padre "App" 
            a nuestro componente hijo "Hello" */}
        <Hello to="Pedro" from="Monterrey" />
        <Hello to="Juan" from="California" />
        <Hello to="Luis" from="Nueva York" />
        <Hello to="Maria" from="Dubai" />
      </div>
    )
  }
}

ReactDOM.render(<App />, document.getElementById("root"))
