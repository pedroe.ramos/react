class Hello extends React.Component {
  render() {
    //variable que guarda la llamada a los props
    //Importante: No pueden ser modifcados o alterados
    const props = this.props
    return (
      <div>
        <h1>
          {/* Llamade de datos que deben aparecer en pantalla */}
          Hola {props.to} de {props.from}
        </h1>
      </div>
    )
  }
}
