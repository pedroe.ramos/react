class App extends React.Component {
  render() {
    return (
      <div>
        {/* Datos que se pasan de nuestro componente padre "App" 
            a nuestro componente hijo "Hello" */}
        {/* Para otro typo de dato que no sea string se debe usar "{}" */}
        <Hello
          to="Pedro"
          from="Monterrey"
          age={21}
          data={[1, 2, 3, 4, 5]}
          signo={3}
          img="https://cdn2.actitudfem.com/media/files/styles/large/public/images/2019/08/de-donde-salio-el-meme-del-gato-en-la-mesa-portada.jpg"
        />
      </div>
    )
  }
}

ReactDOM.render(<App />, document.getElementById("root"))
