class Hello extends React.Component {
  //Props Defaults que se mostraran en pantalla cuando no se tenga ningun dato
  //del componente padre
  static defaultProps = {
    to: "ToDefault",
    from: "FromDefault",
    signo: 1,
    age: 7
  }
  render() {
    //Funcion para repetir el sigo la cantidad de
    //veces que el dato "signo" indique
    let signo = "!".repeat(this.props.signo)
    //variable que guarda la llamada a los props
    //Importante: No pueden ser modifcados o alterados
    const props = this.props
    return (
      <div>
        <h1>
          {/* Llamade de datos que deben aparecer en pantalla */}
          Hola {props.to} de {props.from}
          {signo} Edad: {props.age}
        </h1>
      </div>
    )
  }
}
