class App extends React.Component {
  render() {
    return (
      <div>
        {/* Datos que se pasan de nuestro componente padre "App" 
            a nuestro componente hijo "Hello" */}
        {/* Para otro typo de dato que no sea string se debe usar "{}" */}
        {/* Componente con datos */}
        <Hello to="Pedro" from="Monterrey" age={21} signo={3} />
        {/* Componente sin datos */}
        <Hello />
      </div>
    )
  }
}

ReactDOM.render(<App />, document.getElementById("root"))
