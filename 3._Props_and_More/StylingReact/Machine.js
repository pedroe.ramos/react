class Machine extends React.Component {
  render() {
    //Variables que guarda los datos del componente
    const { s1, s2, s3 } = this.props
    //3- otra forma de agregar estilo a react es guardar los estilos en una variable
    //y colocarla directo en la etiqueta html asi 'Style={NombreVariable}'
    const win = { color: "white", background: "green" }
    const lose = { color: "white", background: "red" }
    return (
      //1- Una forma de agregar estilo a react es desde la hoja de estilos
      //Como "class" es una palabra reservada de React, lo que se hace para agregar
      //una clase a una etiqueta html se coloca con el nombre de "className"
      <div className="Machine">
        {/*2-  Otra forma de agregar estilo a react es ponerlo con el atributo Style*/}
        <p style={{ fontSize: "50px", backgroundColor: "white" }}>
          {s1} {s2} {s3}
        </p>
        {/* Aqui se agregan las variables que guardan los estilos para win y lose */}
        <p style={s1 === s2 && s2 === s3 ? win : lose}>
          {/* Operador Temario */}
          {s1 === s2 && s2 === s3 ? "Ganaste!!!" : "Perdiste!!!"}
        </p>
      </div>
    )
  }
}
