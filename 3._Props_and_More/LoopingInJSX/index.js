class App extends React.Component {
  render() {
    return (
      <div>
        <Friend
          name="Pedro"
          hobbies={["Jugar Soccer", "Escuchar Música", "Comer"]}
        />
      </div>
    )
  }
}

ReactDOM.render(<App />, document.getElementById("root"))
