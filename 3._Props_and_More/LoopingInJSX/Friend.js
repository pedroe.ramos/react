class Friend extends React.Component {
  render() {
    //Constante que guarda los datos que se desplegaran en la pantalla
    const { name, hobbies } = this.props
    return (
      <div>
        <h1>{name}</h1>
        <ul>
          {/* Loop {Array.map(fn)} */}
          {hobbies.map(h => (
            <li>{h}</li>
          ))}
        </ul>
      </div>
    )
  }
}
