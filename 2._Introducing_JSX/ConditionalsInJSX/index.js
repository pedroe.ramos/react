//funcion que devuelve un numero aleatorio del 1 al 10
function getNum() {
  return Math.floor(Math.random() * 10) + 1
}

//Metodo Class con operador temario
class NumPicker extends React.Component {
  render() {
    //constancia que guarda una funcion en una variable
    const num = getNum()
    //let declara una variable de alcance local
    //let msg
    //Condicional if
    //if (num === 7) {
    //  msg = (
    //    <div>
    //      <h2>Felicidades Ganaste</h2>
    //      <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSAK5gIgIp-_YIryEifOTG5bv1pf87peNUTb0oYZx3eFyEgso8r&s"></img>
    //    </div>
    //  )
    //} else {
    //  ;<p>Lo siento perdiste</p>
    //}

    return (
      <div>
        <h1>Tu numero es {num}</h1>
        {/*msg*/}
        {/*{algo ? cadena : expresionboleana}*/}
        <p>{num === 7 ? "Felicidades ganaste" : "Lo siento perdiste"}</p>
        {num === 7 && (
          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSAK5gIgIp-_YIryEifOTG5bv1pf87peNUTb0oYZx3eFyEgso8r&s" />
        )}
      </div>
    )
  }
}

ReactDOM.render(<NumPicker />, document.getElementById("root"))
