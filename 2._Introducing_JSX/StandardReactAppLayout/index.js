//Componente App
//Lo mejor es que se le llame App al componente superior
class App extends React.Component {
  render() {
    return (
      <div>
        {/* Componentes secundarios */}
        {/* Es recomendable que el componente lleve el mismo
            nombre del archivo
            ej. componente: Hello archivo: Hello.js */}
        <Hello />
        <NumPicker />
      </div>
    )
  }
}

ReactDOM.render(<App />, document.getElementById("root"))
