function getNum() {
  return Math.floor(Math.random() * 10) + 1
}

class NumPicker extends React.Component {
  render() {
    const num = getNum()

    return (
      <div>
        <h1>Tu numero es {num}</h1>
        <p>{num === 7 ? "Felicidades ganaste" : "Lo siento perdiste"}</p>
        {num === 7 && (
          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSAK5gIgIp-_YIryEifOTG5bv1pf87peNUTb0oYZx3eFyEgso8r&s" />
        )}
      </div>
    )
  }
}
