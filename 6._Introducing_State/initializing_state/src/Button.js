import React, { Component } from "react"

class Button extends Component {
  render() {
    return (
      <button
        //Evento OnClick
        //onClick={function(e) {alert('mensaje') }}
        //Esta evento tiene una funcion que llamara un alert para avisarnos
        //que hemos clickeado el boton
        onClick={function() {
          alert("Has clickeado el boton")
        }}
      >
        CLICK ME !!!!
      </button>
    )
  }
}

export default Button
