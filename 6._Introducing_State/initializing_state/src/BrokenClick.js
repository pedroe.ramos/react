import React, { Component } from "react"

class BrokenClick extends Component {
  constructor(props) {
    super(props)
    //Establece el estado por default
    this.state = { clicked: false }
    //Enlazamiento de funciones a un componente
    //Se enlazan las funciones, con "".bind",
    //para asegurar que tengan acceso a los atributos del componente
    this.handleClick = this.handleClick.bind(this)
    this.returnClick = this.returnClick.bind(this)
  }
  //Funcion que cambia el estado del boton a true para indicar que se ha clickeado el boton
  handleClick(e) {
    this.setState({ clicked: true })
  }
  //Funcion que cambia el estado del boton a false para indicar que se no ha clickeado el boton
  returnClick(e) {
    this.setState({ clicked: false })
  }
  render() {
    return (
      <div>
        {
          //Lo que tenemos aqui es un H1 que nos mostrara si se ha clickeado el boton o no
        }
        <h1>{this.state.clicked ? "Clickeaste" : "No Clickeaste"}</h1>
        {
          //Con el evento onClick se llama a las funciones para cambiar el estado
        }
        <button onClick={this.handleClick}>CLICK ME!!!</button>
        <br></br>
        <button onClick={this.returnClick}>Atras</button>
      </div>
    )
  }
}

export default BrokenClick
