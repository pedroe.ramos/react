import React, { Component } from "react"

class BrokenClick2 extends Component {
  //Sintaxis alterna
  state = { clicked: false }
  //Funcion que cambia el estado del boton a true para indicar que se ha clickeado el boton
  //Arrow function
  handleClick = e => {
    this.setState({ clicked: true })
  }
  //Funcion que cambia el estado del boton a false para indicar que se no ha clickeado el boton
  //Arrow function
  returnClick = e => {
    this.setState({ clicked: false })
  }
  render() {
    return (
      <div>
        {
          //Lo que tenemos aqui es un H1 que nos mostrara si se ha clickeado el boton o no
        }
        <h1>{this.state.clicked ? "Clickeaste2" : "No Clickeaste2"}</h1>
        {
          //Con el evento onClick se llama a las funciones para cambiar el estado
        }
        <button onClick={this.handleClick}>CLICK ME!!!</button>
        <br></br>
        <button onClick={this.returnClick}>Atras</button>
      </div>
    )
  }
}

export default BrokenClick2
