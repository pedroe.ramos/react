import React, { Component } from "react"

class Game extends Component {
  //Funcion contructor
  //El constructor toma un argumento, props
  constructor(props) {
    super(props)
    //Estados - atributos de instancia del componente
    this.state = {
      score: 0,
      gameover: false
    }
  }

  //Esta es otra forma mas facil de usar states
  //super(props)
  //this.state = {
  //  score: 0,
  //  gameover: false
  //}

  render() {
    return (
      <div>
        {
          //Se llama a los states al igual como en props
          // con la sentencia "this.state.Nombre"
        }
        <h1>Tu puntuacion es: {this.state.score}</h1>
      </div>
    )
  }
}

export default Game
