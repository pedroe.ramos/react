import React, { Component } from "react"

class Clicker extends Component {
  constructor(props) {
    super(props)
    this.state = { num: 1 }
    this.genRandom = this.genRandom.bind(this)
  }
  //Metodo que nos dara un nuemro aleatoriamente
  genRandom() {
    let rand = Math.floor(Math.random() * 10) + 1
    this.setState({ num: rand })
  }
  render() {
    return (
      <div>
        <h1>El numero es: {this.state.num}</h1>
        {
          //Operador Temario para saber que si el numero es 7 hemos ganado
        }
        {this.state.num === 7 ? <h2>GANASTE!!!</h2> : <h2>SIGUE INTENTANDO</h2>}
        <button onClick={this.genRandom}>Click</button>
      </div>
    )
  }
}

export default Clicker
