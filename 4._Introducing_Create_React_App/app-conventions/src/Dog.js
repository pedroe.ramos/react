//importamos componentes desde React
import React, { Component } from "react"
//Importamos la hoja de estilo del componente Dog
//Hacer un archivo CSS para cada componente
import "./Dog.css"
//Importamos la imagen desde la direccion guardada
import ImgDog from "./Dog.jpeg"

//clas "Nombre Componente" extends Component
class Dog extends Component {
  render() {
    return (
      //Se agrega un className para dar estilo a los elementos
      <div className="Dog">
        <h1 className="h1-Dog">DOG!!!</h1>
        {
          //Agregamos con el nombre que importamos
        }
        <img className="ImgDog" src={ImgDog} />
      </div>
    )
  }
}

//Exportamos el componente como el objeto predeterminado
export default Dog
