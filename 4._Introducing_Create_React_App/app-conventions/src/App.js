import React from "react"
//Importamos el componente Dog
//Importante hacer un achivo por componente
import Dog from "./Dog"
import "./App.css"

function App() {
  return (
    <div className="App">
      <Dog />
    </div>
  )
}

export default App
